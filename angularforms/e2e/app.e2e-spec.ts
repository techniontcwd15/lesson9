import { AngularformsPage } from './app.po';

describe('angularforms App', () => {
  let page: AngularformsPage;

  beforeEach(() => {
    page = new AngularformsPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
